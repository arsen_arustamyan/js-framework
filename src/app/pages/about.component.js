import {Component} from "../../core/component";

class AboutComponent extends Component {
    constructor(template, selector) {
        super(template, selector)
    }
}

export const aboutComponent = new AboutComponent(
    `<div class="container"><h1>About Component</h1></div>`,
    'about'
);