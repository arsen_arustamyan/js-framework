import {Component} from "../../core/component";

class HomeComponent extends Component {
    constructor(template, selector, events) {
        super(template, selector, events)
    }
}

const collectionHandler = (ev) => {
    if (ev.target.tagName === 'I') {
        const src = ev.target.getAttribute('data-src');
        document.getElementById('player').src = src;
        document.getElementById('player').play()
    }
};

const events = [
    { element: '.collection', type: 'click', handler: collectionHandler }
];

export const homeComponent = new HomeComponent(
    `<div class="container">
        <h1>Моя музыка</h1>
        <ul class="collection">
          <li class="collection-item"><i class="fa fa-play-circle" aria-hidden="true" data-src="https://cs1-63v4.vk-cdn.net/p4/f98fa22fe1a976.mp3?extra=Z24zCiz5uv4d9ZGaUgjJnlfriRjwhsVdrlAh0NR_lJQOZYRxzKTNZi6TZhrpB4URzQL9ekHj9TftLug7Sz6EyrVxQbkj030hA1LHIa8UunTQYLd0R79olf1a_xaVf5U0G4RdyaGF9CX6"></i> Arayik Avetisyan – Du Im Kyanqn es</li>
        </ul>
     </div>`,
    'home',
    events
);

// https://cs1-63v4.vk-cdn.net/p4/f98fa22fe1a976.mp3?extra=Z24zCiz5uv4d9ZGaUgjJnlfriRjwhsVdrlAh0NR_lJQOZYRxzKTNZi6TZhrpB4URzQL9ekHj9TftLug7Sz6EyrVxQbkj030hA1LHIa8UunTQYLd0R79olf1a_xaVf5U0G4RdyaGF9CX6
