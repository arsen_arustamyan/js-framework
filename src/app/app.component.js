import {Component} from "../core/component";

class AppComponent extends Component {
    constructor(template, selector) {
        super(template, selector)
    }
}

export const appComponent = new AppComponent(
    `<nav class="indigo">
        <div class="nav-wrapper">
          <a href="#" class="brand-logo right">Webogram</a>
          <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
          </ul>
          <audio controls style="margin-top: 17px" src="" id="player">
          
          </audio>
        </div>
      </nav>
    <router>
        <home></home>
    </router>
    `,
    'app'
);