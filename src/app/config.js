import {appComponent} from "./app.component";
import {homeComponent} from "./pages/home.component";
import {aboutComponent} from "./pages/about.component";

const components = [
    homeComponent,
    aboutComponent
];

const routes = {
    '': homeComponent,
    'about': aboutComponent
};

function initRotes() {
    window.addEventListener('hashchange', () => {
        const routeBrowser = window.location.hash.substr(1);
        const route = routes[routeBrowser];
        document.querySelector('router').innerHTML = `<${route.selector}></${route.selector}>`
        route.render();
    });
}

export function start() {
    appComponent.render();
    components.forEach(component => component.render());
    initRotes()
}