export class Component {
    constructor(template, selector, events) {
        this.selector = selector;
        this.template = template;
        this.events = events;
    }

    render() {
        if (document.querySelector(this.selector)) {
            document.querySelector(this.selector).innerHTML = this.template;
            console.warn('render element', this.selector)

            if (this.events) {
                this.events.forEach(e => {
                    document.querySelector(e.element)
                        .addEventListener(e.type, function (ev) {
                            e.handler(ev)
                        })
                })
            }
        }
    }
}